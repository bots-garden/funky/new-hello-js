# Hello JS

> WIP 🚧

```bash
echo -n USER:PASSWORD | base64
{
  "auths": {
    "https://index.docker.io/v1/": {
      "auth": "xxxxxxxxxxxxxxx"
    }
  }
}

export KUBECONFIG=../../cluster/k3s.yaml

kubectl create namespace funky-demo

kubectl create configmap docker-config -n funky-demo --from-file=./config.json

kubectl describe configmap docker-config -n funky-demo-master
kubectl delete configmap docker-config
kubectl delete namespace funky-demo-master
```

## Oauth (Google)

- open the [API Library](https://console.developers.google.com/apis/library)
